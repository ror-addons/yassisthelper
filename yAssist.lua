yAssist = {}
yAssist.target = L""

local tonumber = tonumber
local string_find = string.find

local SetMacroData = SetMacroData
local SendChatText = SendChatText
local WindowSetShowing = WindowSetShowing
local GetIconData = GetIconData
local LabelSetText = LabelSetText
local DynamicImageSetTexture = DynamicImageSetTexture

local playersName = nil
local function GetPlayersName()
	if (playersName == nil) then
	    playersName = (GameData.Player.name)--:match(L"([^^]+)^?([^^]*)");
	end
	return playersName
end

local Squared = Squared

local redirect = 0

local assistMacroId = -1

function yAssist.Initialize()
	CreateWindow("yAssistHelper", true)
	WindowSetShowing("yAssistHelperCareerIcon", false)
	WindowSetShowing("yAssistHelperCareerIconFX", false)
	LayoutEditor.RegisterWindow( "yAssistHelper", L"yAssist", L"yAssistHelper", false, false, true, nil )
	yAssist.WriteLabels(L"")
	
	--[[if ( not yAssist.GetMacroId(L"/script yAssist.Assist()") ) then	-- currently deprecated
		yAssist.CreateMacro(L"yAssist", L"/script yAssist.Assist()", 177)
	end]]--
	if ( not yAssist.GetMacroId(L"/script yAssist.SetAssist()") ) then
		yAssist.CreateMacro(L"ySetAssist", L"/script yAssist.SetAssist()", 193)
	end
	if ( not yAssist.GetMacroId(L"/script yAssist.SetMark()") ) then
		yAssist.CreateMacro(L"ySetAssist", L"/script yAssist.SetMark()", 190)
	end
	assistMacroId = yAssist.GetMacroId(L"Assist")
	if ( not assistMacroId) then
		assistMacroId = yAssist.CreateMacro(L"Assist", L"/assist", 177)
	end
end

function yAssist.Assist()
	if redirect == 0 then 
		if yAssist.target ~= L"" then
			-- Workaround 2
				SystemData.UserInput.selectedGroupMember = yAssist.target
				--PlayerMenuWindow.OnAssist()
				SendChatText(L"/assist "..yAssist.target, L"" )
			--BattlegroupHUD.OnMenuClickAssistMember()
		else
			-- trigger main assist
		end
	elseif redirect == 1 then
		Calling.Target()
	elseif redirect == 2 then
		Enemy.Target()
	end
end

function yAssist.SetMark()
	if redirect == 1 then
		Calling.Call()
	end
	if redirect == 2 then
		Enemy.Mark()
	end
end

function yAssist.SetAssist()
	local actwnd = SystemData.MouseOverWindow.name
	if actwnd == "CallingIcon" or actwnd == "WarBoard_TogglerCalling" then 
		redirect = 1
		DynamicImageSetTexture("yAssistHelperCareerIcon","yAssistRedirect",0,0)
		yAssist.WriteLabels(L"Calling")
		WindowSetShowing("yAssistHelperCareerIcon", true)
		WindowSetShowing("yAssistHelperCareerIconFX", true)
	elseif actwnd == "EnemyIcon" or actwnd == "WarBoard_TogglerEnemy" then
		redirect = 2
		DynamicImageSetTexture("yAssistHelperCareerIcon","yAssistRedirect",0,0)
		yAssist.WriteLabels(L"Enemy")
		WindowSetShowing("yAssistHelperCareerIcon", true)
		WindowSetShowing("yAssistHelperCareerIconFX", true)
	else
		local isSquaredSetter = false
		if Squared then
			local _,_,squaredgrp,squaredmember=string_find(actwnd,"^SquaredUnit_(%d+)_(%d+)Action")
			squaredgrp = tonumber(squaredgrp)
			squaredmember = tonumber(squaredmember)
			if squaredgrp ~= nil and squaredmember ~= nil then 
				isSquaredSetter = true
				local squaredtarget = Squared.GetUnit(squaredgrp,squaredmember)
				if squaredtarget.rawname ~= GetPlayersName() then
					yAssist.target = squaredtarget.rawname
					yAssist.career = squaredtarget.career
					yAssist.SetTexLabel()
					redirect = 0
				end
			end
		end
		if isSquaredSetter == false then
			TargetInfo:UpdateFromClient()
			local ftName = TargetInfo:UnitName ("selffriendlytarget")
			if ftName ~= L"" and ftName ~= GetPlayersName() then
				local ftCareer = TargetInfo:UnitCareer("selffriendlytarget")
				if ftCareer ~= 0 then 
					yAssist.target = ftName
					yAssist.career = ftCareer
					yAssist.SetTexLabel()
				else
					EA_ChatWindow.Print(L"You cannot assist NPCs or pets!")
				end
			else
				redirect = 0
				yAssist.WriteLabels(L"")
				WindowSetShowing("yAssistHelperCareerIcon", false)
				WindowSetShowing("yAssistHelperCareerIconFX", false)
			end
		end
	end
	-- Workaround
	-- rewrite assist macro
	SetMacroData (L"Assist", L"/assist "..yAssist.target, 177, assistMacroId)
end

function yAssist.WriteLabels(text)
	LabelSetText("yAssistHelperLabel", text)
	LabelSetText("yAssistHelperShadow", text)
end

function yAssist.SetTexLabel()
	yAssist.WriteLabels(towstring(yAssist.target))
	local tex,x,y=GetIconData(Icons.GetCareerIconIDFromCareerLine(yAssist.career))
	DynamicImageSetTexture("yAssistHelperCareerIcon",tex,x,y)
	WindowSetShowing("yAssistHelperCareerIcon", true)
	WindowSetShowing("yAssistHelperCareerIconFX", true)
end

---------------------------------------------------------------------------------------------------------------------
local GetMacros = DataUtils.GetMacros
function yAssist.GetMacroId(aWString)
	local macros = GetMacros()
	for slot = 1, EA_Window_Macro.NUM_MACROS
	do
		if macros[slot].text == aWString or macros[slot].name == aWString
		then
			return slot
		end
	end
	return nil
end

function yAssist.CreateMacro(name, text, iconId)
	local slot = yAssist.GetMacroId (text)
	if (slot) then return slot end
	local macros = DataUtils.GetMacros ()
	for slot = 1, EA_Window_Macro.NUM_MACROS
	do
		if (macros[slot].text == L"")
		then
			SetMacroData (name, text, iconId, slot)
 			EA_Window_Macro.UpdateMacros ()
			return slot
		end
	end
	return nil
end
